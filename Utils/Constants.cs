﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public static class Constants
    {
        public static string ImageFolderPath = "";

        public static string VideoFolderPath = "";

        public static string FrontImageFolderPath = "";

        public static string FrontVideoFolderPath = "";

        public static string EmailAdress = "";

        public static string EmailPassword = "";

        public static string EmailFolderPath = "";

        public static string ContactEmailFolderPath = "";

        public static string AdminClientPort = "";

        public static string ClientPort = "";
    }
}
